//
//  MoviesInCategoryCollectionViewCell.swift
//  MoviesApp
//
//  Created by Sara on 12/4/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit

class MoviesInCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movie_poster_image:UIImageView!
}
