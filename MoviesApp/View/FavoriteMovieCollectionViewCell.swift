//
//  FavoriteMovieCollectionViewCell.swift
//  MoviesApp
//
//  Created by Sara on 12/11/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit

class FavoriteMovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var moviePoster:UIImageView?
    @IBOutlet weak var movieName:UILabel?
    @IBOutlet weak var movieDate:UILabel?
    @IBOutlet weak var movieRate:UILabel?
}
