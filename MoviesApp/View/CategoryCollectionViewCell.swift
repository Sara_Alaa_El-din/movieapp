//
//  CategoryCollectionViewCell.swift
//  MoviesApp
//
//  Created by Sara on 12/4/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit
protocol ExpandedCellDelegate:NSObjectProtocol{
    func topButtonTouched(indexPath:IndexPath)
}
class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movie_title_label:UILabel!
    @IBOutlet weak var moview_arrow_btn:UIButton!
    @IBOutlet weak var movieCollectionView:UICollectionView!
    weak var delegate:ExpandedCellDelegate?
    var parentVC:MoviesCategoriesViewController?

    public var indexPath:IndexPath!
    let  urls = URLS()
    let requests = Requests()

    var movies:[MoviePopular]? // movies model popular and top rated
    var movieLatest:MovieLatest? // movies model latest
    var moviesUpComming:[MovieUpComming]? // movies model upcomming and now playing
    var movieViedos:[MovieVideo]? // model for videos
    var movieReviews:[MovieReview]? // model for review
    
    override func select(_ sender: Any?) {
        print("select")
    }
    
    @IBAction func topButtonTouched(_ sender: UIButton) {
        if let delegate = self.delegate{
            delegate.topButtonTouched(indexPath: indexPath)
        }

    }
   
}


extension CategoryCollectionViewCell:UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if movies !=  nil  {
            return movies!.count

        }else if moviesUpComming != nil {
            return moviesUpComming!.count

        }else  if movieLatest != nil{
            return 1
        }else {
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Configure the cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviewCollection", for: indexPath as IndexPath) as!
        MoviesInCategoryCollectionViewCell
        //MARK: check which type of movies
        cell.movie_poster_image.image = nil
        var path:String?
        if movies != nil {
            path =  String(format:"%@%@",urls.sizeOfImage,(self.movies![indexPath.item]).poster_path!)
            
        } else if moviesUpComming != nil {
            path =  String(format:"%@%@",urls.sizeOfImage,(self.moviesUpComming![indexPath.item]).poster_path!)

        }
        else if movieLatest != nil {
            path =  String(format:"%@%@",urls.sizeOfImage,((self.movieLatest)?.poster_path!)!)
        } else {
        }
        

    cell.movie_poster_image.imageFromUrl( urlString: path)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.size.width/3, height: self.frame.size.height);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("select")
        var id:Int?
        if movies != nil {
             id =   (self.movies![indexPath.item]).id!
        } else if moviesUpComming != nil {
              id =  (self.moviesUpComming![indexPath.item]).id!
        }
        else if movieLatest != nil {
            id = (self.movieLatest)?.id!
        } else {
            
        }
        //MARK: requst get videos then reviews for this movie
        requetGetVideos(id: id! , completionHandler: { _ in
            self.requetGetReviews(id: id!, completionHandler: { _ in
                DispatchQueue.main.async {
                    //MARK: redirect to details screen
                    self.redirectToDetils(indexPathRow: indexPath.item)

                }
                
            })
            
        })
        
    }
    
    //MARK: method request for videos

    func requetGetVideos(id:Int,completionHandler:@escaping (Any?) -> (Void)){
        let urlString =  String(format:"%@%d%@?api_key=%@&language=en-US&page=1" ,  urls.url ,id ,urls.videos,urls.apiKey )
        
        requests.getVideosJson(urlString: urlString, completionHandler: {result in
            
            self.movieViedos =  (result as! MovieVideos).results

            completionHandler("success")
          
        })
        
    }
    //MARK: method request for reviews

    func requetGetReviews(id:Int,completionHandler:@escaping (Any?) -> (Void)){
        let urlString =  String(format:"%@%d%@?api_key=%@&language=en-US&page=1" ,  urls.url ,id ,urls.review,urls.apiKey )

        requests.getReviewJson(urlString: urlString, completionHandler: {result in

            self.movieReviews =  (result as! MovieReivews).results

            completionHandler("success")
          
        })
        
    }
    
    
    //MARK: method redirect to details screen
    func redirectToDetils(indexPathRow:Int){
        let movieDetailVC =  self.parentVC?.storyboard?.instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        
        if movies != nil {
            movieDetailVC.movieDetails =  self.movies?[indexPathRow]
        } else if moviesUpComming != nil {
            movieDetailVC.moviesUpCommingDetail = self.moviesUpComming?[indexPathRow]
        }
        else if movieLatest != nil {
            movieDetailVC.movieLatestDetail = self.movieLatest
        } else {
            
        }
        movieDetailVC.movieVideoDetail = self.movieViedos
        movieDetailVC.movieReviewDetail = self.movieReviews
        self.parentVC?.navigationController?.pushViewController(movieDetailVC, animated: true )
    }
    
    
}
