//
//  MoviesCategoryModel.swift
//  MoviesApp
//
//  Created by Sara on 12/7/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation
enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}

struct MoviesPopular {
    let page: Int?
    let total_results: Int?
    let total_pages: Int?
    var results: [MoviePopular]?
    
    init(json: [String: Any]) throws {
        if let page =  json["page"] as? Int {
            self.page = page
            
        }else {
            self.page = 0
            
        }
        if let total_results =  json["total_results"] as? Int{ self.total_results = total_results
            
        }else {
            self.total_results = 0
            
        }
        if let total_pages =  json["total_pages"] as? Int{
            self.total_pages = total_pages

        }else {
            self.total_pages = 0

        }
         let results =  json["results"] as? [[String:Any]]
            
        
        self.results = []
        for result in results!{
            print(result)
            let value = try MoviePopular(json: result)
            self.results?.append(value)
        }
    }
    
    
    
  
  
    
}
struct MoviePopular {
    var id: Int?
    var poster_path: String?
    var video: Bool?
    var vote_average: Double?
    var genre_ids: [NSNumber]?
    var original_title: String?
    var backdrop_path: String?
    var overview: String?
    var original_language: String?
    var vote_count:Int?
    var release_date:String?
   
    init(json: [String: Any]) throws {
        if let id =  json["id"] as? Int {
            self.id = id

        }else {
            self.id = 0

        }
        if let poster_path =  json["poster_path"] as? String{
            self.poster_path = poster_path

        }else {
            self.poster_path = ""

        }
        if let vote_average =  json["vote_average"] as? Double {
            self.vote_average = vote_average

        }else {
            self.vote_average = 0.0

        }
        
        
        if let original_title =  json["original_title"] as? String{
            self.original_title = original_title

        }else {
            self.original_title = ""

        }
        if let release_date =  json["release_date"] as? String {
            self.release_date = release_date
            
        }else {
            self.release_date = ""
            
        }
        let genre_ids =  json["genre_ids"] as? [NSNumber]
        let video =  json["video"] as?Bool
         let backdrop_path =  json["backdrop_path"] as? String
        let overview =  json["overview"] as? String
         let original_language =  json["original_language"] as? String
         let vote_count =  json["vote_count"] as? Int
       
    
    

    self.video = video
    self.genre_ids = genre_ids
    self.backdrop_path = backdrop_path
    self.overview = overview
    self.original_language = original_language
    self.vote_count = vote_count

    }
}
