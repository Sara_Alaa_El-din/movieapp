//
//  MoviesUpComminModel.swift
//  MoviesApp
//
//  Created by Sara on 12/8/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation
struct MoviesUpComming{
   
    var results: [MovieUpComming]?
    
    init(json: [String: Any]) throws {
        let results =  json["results"] as? [[String:Any]]
        
        
        self.results = []
        for result in results!{
            print(result)
            let value = try MovieUpComming(json: result)
            self.results?.append(value)
        }
    }
    
    
    
    
    
    
}
struct MovieUpComming {
    var id: Int?
    var poster_path: String?
    var video: Bool?
    var vote_average: Double?
    var genre_ids: [NSNumber]?
    var original_title: String?
    var backdrop_path: String?
    var overview: String?
    var original_language: String?
    var vote_count:Int?
    var release_date:String?
    var popularity:Double?
    var adult:Bool?
    init(json: [String: Any]) throws {
        if  let id =  json["id"] as? Int{
            self.id = id

        }else {
            self.id = 0

        }
        if let poster_path =  json["poster_path"] as? String{
            self.poster_path = poster_path

        }else {
            self.poster_path = ""

        }
        if  let vote_average =  json["vote_average"] as? Double{
            self.vote_average = vote_average

        }else {
            self.vote_average = 0.0

        }
        let genre_ids =  json["genre_ids"] as? [NSNumber]
        if  let original_title =  json["original_title"] as? String{
            self.original_title = original_title

        }else {
            self.original_title = ""

        }
        if  let release_date =  json["release_date"] as? String{
            self.release_date = release_date
            
        }else {
            self.release_date = ""
            
        }
        let video =  json["video"] as? Bool
        let backdrop_path =  json["backdrop_path"] as? String
        let overview =  json["overview"] as? String
        let original_language =  json["original_language"] as? String
        let vote_count =  json["vote_count"] as? Int
        let popularity =  json["popularity"] as? Double
        let adult = json["adult"] as?Bool
        self.video = video
        self.genre_ids = genre_ids
        self.backdrop_path = backdrop_path
        self.overview = overview
        self.original_language = original_language
        self.vote_count = vote_count
        self.popularity = popularity
        self.adult = adult
    }
    
}
