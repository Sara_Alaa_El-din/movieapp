//
//  MovieVideosModel.swift
//  MoviesApp
//
//  Created by Sara on 12/8/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation

struct MovieVideos {
    var id: Int?
    var results: [MovieVideo]?
    init(json: [String: Any]) throws {
        let id =  json["id"] as? Int
        let results =  json["results"] as? [[String:Any]]
        
        self.id = id
        self.results = []
        for result in results!{
            print(result)
            let value = try MovieVideo(json: result)
            self.results?.append(value)
        }
    }
}


struct MovieVideo {
    var id:String?
    var iso_639_1:String?
    var iso_3166_1:String?
    var key:String?
    var name:String?
    var site:String?
    var size:Int?
    var type:String?
    init(json: [String: Any]) throws {
        if  let id =  json["id"] as? String{
            self.id = id

        }else {
            self.id = ""

        }
        if  let key =  json["key"] as? String{
            self.key = key
            
        }else {
            self.key = ""
            
        }
        let iso_639_1 =  json["iso_639_1"] as? String
        let iso_3166_1 =  json["iso_3166_1"] as? String
        let name =  json["name"] as? String
        let site =  json["site"] as? String
        let size =  json["size"] as? Int
        let type =  json["type"] as? String

        
        
        
        self.iso_639_1 = iso_639_1
        self.iso_3166_1 = iso_3166_1
        self.name = name
        self.site = site
        self.size = size
        self.type = type


        
        
    }
    
    
}
