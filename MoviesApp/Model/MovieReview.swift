//
//  MovieReview.swift
//  MoviesApp
//
//  Created by Sara on 12/8/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation
struct MovieReivews {
    var id: Int?
    var page:Int?
    var results: [MovieReview]?
    init(json: [String: Any]) throws {
        let id =  json["id"] as? Int
        let page =  json["page"] as? Int
        let results =  json["results"] as? [[String:Any]]
        
        self.id = id
        self.page = page
        self.results = []
        for result in results!{
            print(result)
            let value = try MovieReview(json: result)
            self.results?.append(value)
        }
    }
}


struct MovieReview {
    var id:String?
    var author:String?
    var content:String?
    var url:String?
    var total_pages:Int?
    var total_results:Int?
    
    init(json: [String: Any]) throws {
        if let id =  json["id"] as? String{
            self.id = id

        }else {
            self.id = ""

        }
        if  let content =  json["content"] as? String{
            self.content = content

        }else {
            self.content = ""

        }
        
        let author =  json["author"] as? String
        let url =  json["url"] as? String
        let total_pages =  json["total_pages"] as? Int
        let total_results =  json["total_results"] as? Int
        self.author = author
        self.url = url
        self.total_pages = total_pages
        self.total_results = total_results

        
    }
    
  
   
   
  
}
