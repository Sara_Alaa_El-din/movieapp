//
//  MoviesLatestModel.swift
//  MoviesApp
//
//  Created by Sara on 12/8/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation
//
//  MoviesCategoryModel.swift
//  MoviesApp
//
//  Created by Sara on 12/7/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import Foundation



struct MovieLatest {
    var id: Int?
    var poster_path: String?
    var video: Bool?
    var vote_average: Double?
    var original_title: String?
    var backdrop_path: String?
    var overview: String?
    var original_language: String?
    var vote_count:Int?
    var release_date:String?
    var adult :Bool?
    var belongs_to_collection:String?
    var homepage:String?
    var imdb_id:String?
    var production_companies:[String]?
    var production_countries:[String]?
    var revenue:Double?
    var runtime:String?
    var spoken_languages:[String]?
    var status:String?
    var tagline:String?
    var title:String?
    init(json: [String: Any]) throws {
        if let id =  json["id"] as? Int{
            self.id = id

        }else {
            self.id = 0

        }
        
        if let poster_path =  json["poster_path"] as? String {
            self.poster_path = poster_path
        }else {
            self.poster_path = ""
        }
        
        
        if  let vote_average =  json["vote_average"] as? Double{
            self.vote_average = vote_average

        }else {
            self.vote_average = 0.0

            
        }
       
        if let original_title =  json["original_title"] as? String{
            self.original_title = original_title

        }else {
            self.original_title = ""

        }
        let vote_count =  json["vote_count"] as? Int
        if let release_date =  json["release_date"] as? String{
            self.release_date = release_date
            
        }else {
            self.release_date = ""
            
        }
        let video =  json["video"] as? Bool
         let backdrop_path =  json["backdrop_path"] as? String
         let overview =  json["overview"] as? String
         let original_language =  json["original_language"] as? String
         let adult =  json["adult"] as? Bool
         let belongs_to_collection =  json["belongs_to_collection"] as? String
         let homepage =  json["homepage"] as? String
         let imdb_id =  json["imdb_id"] as? String
         let production_companies =  json["production_companies"] as? [String]
         let production_countries =  json["production_countries"] as? [String]
         let revenue =  json["revenue"] as? Double
         let runtime =  json["runtime"] as? String
         let spoken_languages =  json["spoken_languages"] as? [String]
         let status =  json["status"] as? String
         let tagline =  json["tagline"] as? String
         let title =  json["title"] as? String
        
        self.video = video
        self.backdrop_path = backdrop_path
        self.overview = overview
        self.original_language = original_language
        self.vote_count = vote_count
        self.adult = adult
        self.imdb_id =  imdb_id
        self.belongs_to_collection = belongs_to_collection
        self.homepage = homepage
        self.production_companies = production_companies
        self.production_countries = production_countries
        self.revenue = revenue
        self.runtime = runtime
        self.spoken_languages = spoken_languages
        self.status = status
        self.tagline = tagline
        self.title = title
        
    }
    
}
