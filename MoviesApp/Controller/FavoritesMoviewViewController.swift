//
//  FavoritesMoviewViewController.swift
//  MoviesApp
//
//  Created by Sara on 12/11/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit
import CoreData
class FavoritesMoviewViewController: UIViewController {
    var favorietMoviesReturend: [NSManagedObject] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        favorietMoviesReturend = self.fetchFromCoreData()
        
    }

  
    @IBAction func backBarBtn(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FavoritesMoviewViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return favorietMoviesReturend.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Configure the cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoriteCollection", for: indexPath as IndexPath) as!
        FavoriteMovieCollectionViewCell
        cell.tag = ((favorietMoviesReturend[indexPath.item]).value(forKey: "movieId") as? Int)!
        cell.movieName?.text = (favorietMoviesReturend[indexPath.item]).value(forKey: "movieTitle") as? String
        cell.movieRate?.text = (favorietMoviesReturend[indexPath.item]).value(forKey: "movieRate") as? String

        cell.movieDate?.text = (favorietMoviesReturend[indexPath.item]).value(forKey: "movieDate") as? String

        cell.moviePoster?.imageFromUrl( urlString: (favorietMoviesReturend[indexPath.item]).value(forKey: "moviePoster") as? String)
     
        
        return cell
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
}
}
