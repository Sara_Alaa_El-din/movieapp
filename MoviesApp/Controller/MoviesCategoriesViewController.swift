//
//  MoviesCategoriesViewController.swift
//  MoviesApp
//
//  Created by Sara on 12/4/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit



class MoviesCategoriesViewController: UIViewController ,ExpandedCellDelegate{
    @IBOutlet weak var categoryCollectionView:UICollectionView!
    var expandedCellIdentifier = "ExpandableCell"
    var cellWidth:CGFloat{
        return categoryCollectionView.frame.size.width
    }
    
    var expandedHeight : CGFloat = 286
    var notExpandedHeight : CGFloat = 50
    var isExpanded = [Bool]()
    let requests = Requests()
    let  urls = URLS()
    var movies:[MoviePopular]?
    var movieLatest:MovieLatest?
    var moviesUpComming:[MovieUpComming]?
    let movieTypeArray = [MovieApi.Popular.movieApiValue.type , MovieApi.UpComing.movieApiValue.type , MovieApi.NowPlaying.movieApiValue.type , MovieApi.TopRated.movieApiValue.type, MovieApi.Latest.movieApiValue.type]

    let movieArray = [MovieApi.Popular.movieApiValue.name , MovieApi.UpComing.movieApiValue.name , MovieApi.NowPlaying.movieApiValue.name , MovieApi.TopRated.movieApiValue.name, MovieApi.Latest.movieApiValue.name]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        isExpanded = Array(repeating: false, count: movieArray.count)
        self.showActivityIndicatory(uiView: categoryCollectionView)


    }
    
    
 
    func topButtonTouched(indexPath: IndexPath) {
        for i  in  0 ..< isExpanded.count {
            if i == indexPath.item {
                isExpanded[i] = !isExpanded[i]

            }else {
                if isExpanded[i] == true {
                    isExpanded[i] = !isExpanded[i]
                }

            }
        }
        UIView.animate(withDuration: 0.1, delay: 0.1, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.5, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.categoryCollectionView.reloadItems(at: [indexPath])
        }, completion: { success in
            print("success")
        })
    }
    
    @IBAction func favoriteBtn(_ sender:UIBarButtonItem){
        let favoriteMoviesVC =  self.storyboard?.instantiateViewController(withIdentifier: "FavoritesMoviewViewController") as! FavoritesMoviewViewController
        self.navigationController?.pushViewController(favoriteMoviesVC, animated: true )

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension MoviesCategoriesViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
       return movieArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Configure the cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollection", for: indexPath as IndexPath) as!
        CategoryCollectionViewCell
        cell.movie_title_label.text = self.movieArray[indexPath.item]
        if isExpanded[indexPath.item] == true {
            cell.moview_arrow_btn.setImage(UIImage(named: "arrow_up"), for: .normal)

        }else {
            cell.moview_arrow_btn.setImage(UIImage(named: "arrow_down"), for: .normal)

        }
        self.requetGetMovies(index: indexPath.item, completionHandler: {_ in
            cell.movies = self.movies
            cell.moviesUpComming = self.moviesUpComming
            cell.movieLatest = self.movieLatest
            cell.indexPath = indexPath
            cell.delegate = self
            cell.parentVC = self 
            DispatchQueue.main.async {
                cell.movieCollectionView.reloadData()
                self.hideActivityIndecator(uiView: self.categoryCollectionView)

            }
            
        })
  
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isExpanded[indexPath.item] == true{
            return CGSize(width: cellWidth, height: expandedHeight)
        }else{
            return CGSize(width: cellWidth, height: notExpandedHeight)
        }    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
  
    
    func requetGetMovies(index:Int,completionHandler:@escaping (Any?) -> (Void)){
        let urlString =  String(format:"%@%@?api_key=%@&language=en-US&page=1" ,  urls.url ,movieTypeArray[index] ,urls.apiKey )
        DispatchQueue.global().async {
            self.movies = nil
            self.movieLatest = nil
             self.moviesUpComming = nil

            self.requests.getJson(urlString: urlString ,indexOfMovieType:index,completionHandler:{ result in
            
            switch index{
            case MovieApi.Popular.hashValue,MovieApi.TopRated.hashValue:
                self.movies = (result as! MoviesPopular).results!
                
            case MovieApi.NowPlaying.hashValue , MovieApi.UpComing.hashValue :
                self.moviesUpComming = (result as! MoviesUpComming).results
            case MovieApi.Latest.hashValue:
                self.movieLatest = (result as! MovieLatest)
                
                
            default:
                
                print("non")
            }
            completionHandler("success")

        })
        
    }
    }

    
}
