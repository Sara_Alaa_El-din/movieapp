//
//  MovieDetailsViewController.swift
//  MoviesApp
//
//  Created by Sara on 12/8/18.
//  Copyright © 2018 CreativeMinds. All rights reserved.
//

import UIKit
import CoreData
class MovieDetailsViewController: UIViewController {
    let  urls = URLS()
    var movieDetails:MoviePopular? // movies model popular and top rated
    var movieLatestDetail:MovieLatest?// movies model latest
    var moviesUpCommingDetail:MovieUpComming?// movies model upcomming and now playing
    var movieVideoDetail:[MovieVideo]?// model for videos
    var movieReviewDetail:[MovieReview]?// model for review
    @IBOutlet weak var favoriteBtn:UIButton?
    @IBOutlet weak var videoWebView:UIWebView?
    @IBOutlet weak var moviePoster:UIImageView?
    @IBOutlet weak var movieTitle:UIBarButtonItem?
    @IBOutlet weak var movieName:UILabel?
    @IBOutlet weak var movieDate:UILabel?
    @IBOutlet weak var movieRate:UILabel?
    @IBOutlet weak var movieReview:UITextView?
    @IBOutlet weak var videoTrailerWebView:UIWebView?

    var dateValue:String? // variable to store date of movie 
    var nameValue: String? // variable to store name of movie
    var rateValue:String? // variable to store rate of movie
    var path:String? // variable to store path of movie poster
    var movieId:Int? // variable to store movie id
    

    @IBAction func backBarBtn(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadMovieDetail()
        
    }

    //MARK: method to load modie details
    func loadMovieDetail(){
        
        if (movieVideoDetail?.count)! > 0{
            loadYoutube(videoID: ((movieVideoDetail![0]).key)! , wv: videoWebView!)
            loadYoutube(videoID: ((movieVideoDetail![1]).key)! , wv: videoTrailerWebView!)

        }

        if (movieReviewDetail?.count)! > 0{
            movieReview?.text = ((movieReviewDetail![0]).content)!

        }
        
        if movieDetails !=  nil{
            movieId = movieDetails?.id
            nameValue = movieDetails?.original_title
            rateValue = String((movieDetails?.vote_average)! )
            dateValue =  movieDetails?.release_date
            path =  String(format:"%@%@",urls.sizeOfImage,(movieDetails?.poster_path!)!)

        }else if moviesUpCommingDetail != nil{
            movieId = moviesUpCommingDetail?.id
            nameValue = moviesUpCommingDetail?.original_title
            rateValue = String((moviesUpCommingDetail?.vote_average)! )
            dateValue =  moviesUpCommingDetail?.release_date
            path =  String(format:"%@%@",urls.sizeOfImage,(moviesUpCommingDetail?.poster_path!)!)


        }else if movieLatestDetail != nil {
            movieId = movieLatestDetail?.id
            nameValue = movieLatestDetail?.original_title
            rateValue = String((movieLatestDetail?.vote_average)! )
            dateValue =  movieLatestDetail?.release_date
            path =  String(format:"%@%@",urls.sizeOfImage,(movieLatestDetail?.poster_path!)!)


        }else {
            
        }
        
        movieName?.text = nameValue
        movieRate?.text = rateValue
        movieTitle?.title = nameValue
        movieDate?.text = dateValue
        moviePoster?.imageFromUrl( urlString: path)

        
       let favorietMoviesReturend = self.fetchFromCoreData()
        
        for movie in favorietMoviesReturend{
            if movie.value(forKey: "movieId")as? Int == movieId{
                self.favoriteBtn?.setImage(UIImage(named: "004-heart-2"), for: .normal)

            }
        }

        
    }
    func loadYoutube(videoID:String,wv:UIWebView) {
        guard
            let youtubeURL = URL(string: "\(urls.youTubeViedoBasicUrl)\(videoID)")
            else { return }
        wv.loadRequest( URLRequest(url: youtubeURL) )
        wv.transform = CGAffineTransform(scaleX: 1, y: 1);
        self.view.sendSubview(toBack: wv)

    }
    
    @IBAction func favoriBtn(_ sender:UIButton){
        if favoriteBtn?.image(for: .normal) == UIImage(named: "003-heart-1"){
            
            //MARK: method to store favorite movie in coreData
            storeInCoreData(movieTitle: nameValue!, moviePoster: path!, movieRate: rateValue!, movieReview:(movieReview?.text)! , movieTrailer1:((movieVideoDetail![0]).key)! , movieTrailer2: ((movieVideoDetail![1]).key)!, movieDate: dateValue!, movieId: movieId!, onCompilation: { _ in 
                self.favoriteBtn?.setImage(UIImage(named: "004-heart-2"), for: .normal)

            })
            
        }else {
            //MARK: delete favorite movie from coreData
            deleteFromCoreData(movieId: movieId!, onCompilation: {_ in
                self.favoriteBtn?.setImage(UIImage(named: "003-heart-1"), for: .normal)

            })
            

        }
    }
    
    //MARK: action on share button
    @IBAction func shareBtn(_ sender:UIBarButtonItem){
        
        let textToShare = "Movie"
        
        if let myWebsite = NSURL(string: urls.url) {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let   activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.completionWithItemsHandler = { activity, success, items, error in
                if !success{
                    print("successed")
                }
            }
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




