//
//  Requests.swift
//  DemoForMap
//
//  Created by Sara on 11/1/18.
//  Copyright © 2018 MadarSoft. All rights reserved.
//

import UIKit

class Requests: NSObject {
    let sharedInstance = URLS()
    let global = GlobalMethods()
    

    
  
    
    func getJson(urlString: String,indexOfMovieType:Int, completionHandler: @escaping (Any?) -> (Void)) {
//        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString)
        
        print("URL being used is \(url!)")
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { data, response, error in
            if let data = data {
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                print("request completed with code: \(statusCode)")
                if (statusCode == 200) {
                do {



                    let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            
                if let json = jsonSerialized {
                            print(json)
                    switch indexOfMovieType{
                    case MovieApi.Popular.hashValue,MovieApi.TopRated.hashValue:
                        let result = try MoviesPopular(json: json as! [String : Any])
                        completionHandler(result)
                    case MovieApi.NowPlaying.hashValue , MovieApi.UpComing.hashValue :
                        let result = try MoviesUpComming(json: json as! [String : Any])
                        completionHandler(result)

                    case MovieApi.Latest.hashValue:
                        let result = try MovieLatest(json: json as! [String : Any])
                        completionHandler(result)
                        
                        
                    default:
                        print("non")
                    }
                    

                                            }
                    print("return to completion handler with the data")
//                    let results =  try JSONDecoder().decode(APIResults.self, from:data )
//                    if let json = results{
//                        completionHandler(results)

//                    }
                   
                            }  catch let error as NSError {
                                                print(error.localizedDescription)
                            }

                }
            } else if let error = error {
                print("***There was an error making the HTTP request***")
                print(error.localizedDescription)
                completionHandler(nil)
            }
        }
        task.resume()
    }
    
    
    func getVideosJson(urlString: String, completionHandler: @escaping (Any?) -> (Void)) {
        //        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString)
        
        print("URL being used is \(url!)")
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { data, response, error in
            if let data = data {
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                print("request completed with code: \(statusCode)")
                if (statusCode == 200) {
                    do {
                        
                        
                        
                        let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                        
                        if let json = jsonSerialized {
                            print(json)
                            let result = try MovieVideos(json: json as! [String : Any])
                            completionHandler(result)

                            
                        }
                        print("return to completion handler with the data")
                        //                    let results =  try JSONDecoder().decode(APIResults.self, from:data )
                        //                    if let json = results{
                        //                        completionHandler(results)
                        
                        //                    }
                        
                    }  catch let error as NSError {
                        print(error.localizedDescription)
                    }
                    
                }
            } else if let error = error {
                print("***There was an error making the HTTP request***")
                print(error.localizedDescription)
                completionHandler(nil)
            }
        }
        task.resume()
    }
    
    
    func getReviewJson(urlString: String, completionHandler: @escaping (Any?) -> (Void)) {
        //        let urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: urlString)
        
        print("URL being used is \(url!)")
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { data, response, error in
            if let data = data {
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                print("request completed with code: \(statusCode)")
                if (statusCode == 200) {
                    do {
                        
                        
                        
                        let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
                        
                        if let json = jsonSerialized {
                            print(json)
                            let result = try MovieReivews(json: json as! [String : Any])
                            completionHandler(result)
                            
                            
                        }
                        print("return to completion handler with the data")
                        //                    let results =  try JSONDecoder().decode(APIResults.self, from:data )
                        //                    if let json = results{
                        //                        completionHandler(results)
                        
                        //                    }
                        
                    }  catch let error as NSError {
                        print(error.localizedDescription)
                    }
                    
                }
            } else if let error = error {
                print("***There was an error making the HTTP request***")
                print(error.localizedDescription)
                completionHandler(nil)
            }
        }
        task.resume()
    }
    
    
    
 
}
