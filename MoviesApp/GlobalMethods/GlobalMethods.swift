//
//  GlobalMethods.swift
//  DemoForMap
//
//  Created by Sara on 11/1/18.
//  Copyright © 2018 MadarSoft. All rights reserved.
//

import UIKit
import CoreData

class GlobalMethods: NSObject {
    
    func returnSearchURLPath(baseURL:String ,lat:Double , long:Double ,distance:Int, locationType:String, apiKeyForMap:String)->String{
        let urlpath = "\(baseURL)\(lat),\(long)&radius=\(distance)&types=\(locationType)&sensor=true&key=\(apiKeyForMap)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        return urlpath!
    }
    
    func returnGetLocationURLPath(baseUrl:String ,apiKey:String ,origin:String ,destination:String ,drivingOrWaking:String)->String{
        let urlPath =  "\(baseUrl)\(apiKey)&sensor=true&origin=\(origin)&destination=\(destination)&mode=\(drivingOrWaking)"
        
        return urlPath
    }
    
    
    
    
}
extension UIImageView {
    //MARK: method to get image with url
    public func imageFromUrl( urlString: String?) {

        let base  = URL(string: "http://image.tmdb.org/t/p/")

        if let imageUrl = URL(string: urlString! , relativeTo:base ){
            do {
                let data =  try Data(contentsOf: imageUrl)
                if let imageData = data as Data? {
                    self.image = UIImage(data: imageData)
                }

            }
            catch {
                print("error")
                self.image = UIImage(named: "noImage")

            }


        }else {
            self.image = UIImage(named: "noImage")

        }
    }
}


extension UIViewController{

    //MARK: method to show indicator
    func showActivityIndicatory(uiView: UIView) {
            let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            let container: UIView = UIView()
            container.frame = CGRect(x: 0, y: 0, width: 80, height: 80) // Set X and Y whatever you want
            container.backgroundColor = UIColor.clear
           container.tag = 9999999
            activityView.center = uiView.center
            
            
            container.addSubview(activityView)
            uiView.addSubview(container)
//            uiView.bringSubview(toFront: container)
            activityView.startAnimating()
        
      
    }
    //MARK: method to hide indicator
    func hideActivityIndecator(uiView: UIView ){
            if let viewWithTag = uiView.viewWithTag(9999999) {
                viewWithTag.removeFromSuperview()
            }
        
    }
    
    //MARK: method to store in coreData
    func storeInCoreData(movieTitle:String , moviePoster:String,movieRate:String,movieReview:String,movieTrailer1:String, movieTrailer2:String , movieDate:String ,movieId:Int , onCompilation:@escaping (_ complete:String)->Void){
        var favorietMovies: [NSManagedObject] = []

        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        
        let entity =
            NSEntityDescription.entity(forEntityName: "FvoriteMovie",
                                       in: managedContext)!
        
        let favorietMovie = NSManagedObject(entity: entity,
                                            insertInto: managedContext)
        
        favorietMovie.setValue(movieTitle, forKeyPath: "movieTitle")
        favorietMovie.setValue(moviePoster, forKeyPath: "moviePoster")
        favorietMovie.setValue(movieRate, forKeyPath: "movieRate")
        favorietMovie.setValue(movieReview, forKeyPath: "movieReview")
        favorietMovie.setValue(movieTrailer1, forKeyPath: "movieTrailer1")
        favorietMovie.setValue(movieTrailer2, forKeyPath: "movieTrailer2")
        favorietMovie.setValue(movieDate, forKeyPath: "movieDate")
        favorietMovie.setValue(movieId, forKey: "movieId")
        
        do {
            try managedContext.save()
            favorietMovies.append(favorietMovie)
            onCompilation("success store data")
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
    //MARK: mehtod to fethc from coreData
    func fetchFromCoreData()->[NSManagedObject]{
        var favorietMovies: [NSManagedObject] = []

        let appDelegate =
            UIApplication.shared.delegate as? AppDelegate

        let managedContext =
            appDelegate?.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FvoriteMovie")
        
        do {
            favorietMovies = (try managedContext?.fetch(fetchRequest))!

        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return favorietMovies

    }
    
    
    //MARK: remove favorite movie
    func deleteFromCoreData(movieId:Int ,onCompilation:@escaping (_ complete:String)->Void){
        let appDelegate =
            UIApplication.shared.delegate as? AppDelegate

        let managedContext =
            appDelegate?.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "FvoriteMovie")
        
        do {
            
            let favorietMovies = (try managedContext?.fetch(fetchRequest))!
            for object in favorietMovies {
                if object.value(forKey: "movieId")as? Int == movieId{
                managedContext?.delete(object)
                }
            }
            try managedContext?.save()
            onCompilation("success delete")
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }

        
        
        
        
    }
    
}

@IBDesignable extension UIView{
    
    
    @IBInspectable var viewBorderColor :UIColor{
        
        get {
            return self.viewBorderColor
        }
        set(borderColor){
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var viewBorderWidth :CGFloat{
        
        get {
            return self.layer.borderWidth
        }
        set(borderWidth){
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var viewShadowColor:UIColor{
        get{
            return UIColor(cgColor: layer.shadowColor!)
        }
        set(viewShadowColor){
            layer.shadowColor = viewShadowColor.cgColor
        }
    }
    
    @IBInspectable var viewShadowOpacity:Float{
        get{
            return layer.shadowOpacity
        }
        set(viewShadowOpacity){
            layer.shadowOpacity = viewShadowOpacity
        }
    }
    
    @IBInspectable var viewShadowRadius:CGFloat{
        get {
            return layer.shadowRadius
        }
        set(viewShadowRadius){
            layer.shadowRadius = viewShadowRadius
        }
    }
    @IBInspectable var viewShadowOffeset:CGSize{
        get {
            return layer.shadowOffset
        }
        set(viewShadowOffest){
            layer.shadowOffset = viewShadowOffest
        }
    }
    @IBInspectable var viewCornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    @IBInspectable var maskToBounds: Bool {
        set(viewMaskToBounds) {
            layer.masksToBounds = viewMaskToBounds
        }
        get {
            return layer.masksToBounds
        }
    }
    
    
}

