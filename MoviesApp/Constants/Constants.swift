//
//  Constants.swift
//  DemoForMap
//
//  Created by Sara on 11/1/18.
//  Copyright © 2018 MadarSoft. All rights reserved.
//

import Foundation
struct URLS {
    let apiKey = "e01ba428f71f6f8bdb97cc612880d8c2"
    let baseURL = "​http://image.tmdb.org/t/p/"
    let sizeOfImage = "w185"
    let url = "https://api.themoviedb.org/3/movie/"
    let videos = "/videos"
    let review = "/reviews"
    let youTubeViedoBasicUrl = "https://www.youtube.com/watch?v="
}

public enum MovieApi {
    case Popular
    case UpComing 
    case NowPlaying
    case TopRated
    case Latest
    
    var movieApiValue :(name:String , type:String){
        switch self {
        case .Popular:
            return ("Popular","popular")
        case .UpComing:
            return ("UpComing","upcoming")
        case .NowPlaying:
                return ("NowPlaying","now_playing")
        case .TopRated:
            return ("TopRated","top_rated")
        case .Latest:
                return ("Latest","latest")
        
        }
    }
    
}




